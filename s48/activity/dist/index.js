"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const formAdd = document.querySelector('#form-add');
const formEdit = document.querySelector('#form-edit');
const title = document.querySelector('#title');
const body = document.querySelector('#body');
const editId = document.querySelector('#edit-id');
const editTitle = document.querySelector('#edit-title');
const editBody = document.querySelector('#edit-body');
const updateBtn = document.querySelector('#btn-update');
const postList = document.querySelector('#div-post-entries');
const BASE_URL = 'https://jsonplaceholder.typicode.com';
const getAllPosts = () => __awaiter(void 0, void 0, void 0, function* () {
    const res = yield fetch(BASE_URL + '/posts');
    return res.json();
});
const createPost = (newPost) => __awaiter(void 0, void 0, void 0, function* () {
    const init = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newPost),
    };
    const res = yield fetch(BASE_URL + '/posts', init);
    return res.json();
});
const updatePost = (updatedPost) => __awaiter(void 0, void 0, void 0, function* () {
    const init = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(updatedPost),
    };
    const res = yield fetch(BASE_URL + '/posts/' + updatedPost.id, init);
    return res.json();
});
const removePost = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const init = { method: 'DELETE' };
    yield fetch(BASE_URL + '/posts/' + id, init);
});
const editPost = (id) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const posts = yield getAllPosts();
    const post = posts.find(p => p.id === Number(id));
    editId.value = (_a = post === null || post === void 0 ? void 0 : post.id) === null || _a === void 0 ? void 0 : _a.toString();
    editTitle.value = post === null || post === void 0 ? void 0 : post.title;
    editBody.value = post === null || post === void 0 ? void 0 : post.body;
    updateBtn.removeAttribute('disabled');
});
const deletePost = (id) => {
    var _a;
    removePost(id);
    (_a = document.querySelector(`#post-${id}`)) === null || _a === void 0 ? void 0 : _a.remove();
};
const renderPosts = () => __awaiter(void 0, void 0, void 0, function* () {
    const posts = yield getAllPosts();
    const postElement = posts
        .map(post => `
        <div id ="post-${post.id}">
          <h3 id="post-title-${post.id}">${post.title}</h3>
          <p id="post-body-${post.id}">${post.body}</p>
          <button onClick="editPost('${post.id}')">Edit</button>
          <button onClick="deletePost('${post.id}')">Delete</button>
        </div>
      `)
        .join('');
    postList.innerHTML = postElement;
});
formAdd.addEventListener('submit', (e) => __awaiter(void 0, void 0, void 0, function* () {
    e.preventDefault();
    const newPost = yield createPost({
        title: title.value,
        body: body.value,
        userId: 1,
    });
    console.log(newPost);
    alert('Successfully added!');
    formAdd.reset();
}));
formEdit.addEventListener('submit', (e) => __awaiter(void 0, void 0, void 0, function* () {
    e.preventDefault();
    const updatedPost = yield updatePost({
        id: Number(editId.value),
        title: editTitle.value,
        body: editBody.value,
        userId: 1,
    });
    console.log(updatedPost);
    updateBtn.setAttribute('disabled', 'disabled');
    alert('Successfully updated!');
    formEdit.reset();
}));
renderPosts();

import { User } from '../stores/authStore';
import apiClient from './apiClient';

export interface LoginFormData {
  email: string;
  password: string;
}

export interface LoginResponseData {
  access: string;
}

interface RegisterFormData {
  firstName: string;
  lastName: string;
  email: string;
  mobileNo: string;
  password: string;
}

export const loginUser = (userData: LoginFormData) => {
  return apiClient
    .post<LoginResponseData>('/users/login', userData)
    .then(({ data }) => data);
};

export const checkEmail = (email: string) => {
  return apiClient
    .post('/users/checkEmail', { email })
    .then(({ data }) => data);
};

export const registerUser = (user: RegisterFormData) => {
  return apiClient.post('/users/register', user).then(({ data }) => data);
};

export const getUserInfo = (token: string) => {
  const controller = new AbortController();

  const headers = { Authorization: `Bearer ` + token };
  const request = apiClient
    .get<User>('/users/details', { signal: controller.signal, headers })
    .then(({ data }) => data);

  return { request, cancel: () => controller.abort() };
};

export const enroll = (courseId: string) => {
  return apiClient.post('/users/enroll', { courseId }).then(({ data }) => data);
};

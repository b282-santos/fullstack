import apiClient from './apiClient';

export interface Course {
  _id: string;
  name: string;
  description: string;
  price: number;
  enrollees: [];
}

export const getActiveCourses = () => {
  return apiClient.get<Course[]>('/courses/active').then(({ data }) => data);
};

export const getCourse = (id: string) => {
  return apiClient.get<Course>('/courses/' + id).then(({ data }) => data);
};

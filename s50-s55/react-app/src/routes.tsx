import { createBrowserRouter } from 'react-router-dom';
import App from './App';
import CourseList from './components/CourseList';
import CourseView from './components/CourseView';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import Register from './pages/Register';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      { index: true, element: <Home /> },
      {
        path: 'courses',
        element: <Courses />,
        children: [
          { index: true, element: <CourseList /> },
          { path: ':id', element: <CourseView /> },
        ],
      },
      { path: 'login', element: <Login /> },
      { path: 'register', element: <Register /> },
      { path: '*', element: <PageNotFound /> },
    ],
  },
]);

export default router;

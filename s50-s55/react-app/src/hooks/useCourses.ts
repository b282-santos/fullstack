import { useQuery } from '@tanstack/react-query';
import { Course, getActiveCourses } from '../services/courseService';

const useCourses = () => {
  return useQuery<Course[], Error, Course[]>({
    queryKey: ['courses'],
    queryFn: getActiveCourses,
  });
};

export default useCourses;

import { create } from 'zustand';

export interface User {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  isAdmin: boolean;
  mobileNo: string;
  enrollements: [];
}

interface States {
  user: User;
}

interface Actions {
  login: (user: User) => void;
  logout: () => void;
}

const useAuthStore = create<States & Actions>(set => ({
  user: {} as User,
  login: user => set(() => ({ user: { ...user } })),
  logout: () =>
    set(() => {
      localStorage.removeItem('token');
      return { user: {} as User };
    }),
}));

export default useAuthStore;

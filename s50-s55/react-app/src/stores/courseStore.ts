import { create } from 'zustand';
import { Course } from '../services/courseService';

interface States {
  course: Course;
}

interface Actions {
  setCourse: (course: Course) => void;
}

const useCourseStore = create<States & Actions>(set => ({
  course: {} as Course,
  setCourse: course => set(() => ({ course })),
}));

export default useCourseStore;

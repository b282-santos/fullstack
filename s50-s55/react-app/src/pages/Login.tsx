import { Button, Form } from 'react-bootstrap';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Navigate } from 'react-router-dom';
import { LoginFormData, getUserInfo, loginUser } from '../services/userService';
import useAuthStore from '../stores/authStore';
import { loginFailed, loginSuccess } from '../utils/modals';

const Login = () => {
  const { user, login } = useAuthStore();
  const {
    register,
    handleSubmit,
    reset,
    formState: { isValid },
  } = useForm<LoginFormData>();

  const onSubmit: SubmitHandler<LoginFormData> = async formData => {
    try {
      const { access: token } = await loginUser(formData);
      const { request } = getUserInfo(token);
      const user = await request;
      localStorage.setItem('token', token);
      login(user);
      loginSuccess();
    } catch (err) {
      loginFailed();
    }

    reset();
  };

  if (user._id) return <Navigate to="/" />;

  return (
    <Form className="mx-auto" onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Email"
          {...register('email', { required: true })}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          {...register('password', { required: true })}
        />
      </Form.Group>

      <Button variant="success" type="submit" disabled={!isValid}>
        Login
      </Button>
    </Form>
  );
};

export default Login;

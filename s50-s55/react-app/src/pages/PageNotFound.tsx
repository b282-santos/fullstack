import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../components/Banner';

const PageNotFound = () => {
  return (
    <Banner>
      <h1>Error 404 - Page not found.</h1>
      <p>The page you are looking for cannot be found.</p>
      <Link to="/">
        <Button>Back to Home</Button>
      </Link>
    </Banner>
  );
};

export default PageNotFound;

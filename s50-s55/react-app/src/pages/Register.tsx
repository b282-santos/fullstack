import { Button, Form } from 'react-bootstrap';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Navigate, useNavigate } from 'react-router-dom';
import { checkEmail, registerUser } from '../services/userService';
import useAuthStore from '../stores/authStore';
import { emailExists, registerSuccessfull } from '../utils/modals';

interface RegisterFormData {
  firstName: string;
  lastName: string;
  email: string;
  mobileNo: string;
  password: string;
  verifyPassword: string;
}

const Register = () => {
  const navigate = useNavigate();
  const { user } = useAuthStore();
  const {
    register,
    handleSubmit,
    reset,
    watch,
    formState: { isValid },
  } = useForm<RegisterFormData>();

  const onSubmit: SubmitHandler<RegisterFormData> = async data => {
    const { verifyPassword, ...formData } = data;
    checkEmail(data.email)
      .then(() => emailExists())
      .catch(async () => {
        await registerUser(formData);
        registerSuccessfull();
        navigate('/login');
        reset();
      });
  };

  if (user._id) return <Navigate to="/" />;

  return (
    <Form className="mx-auto" onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          {...register('firstName', { required: true })}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          {...register('lastName', { required: true })}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" {...register('email', { required: true })} />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          {...register('mobileNo', {
            required: true,
            minLength: 11,
            maxLength: 11,
          })}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          {...register('password', { required: true })}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="verifyPassword">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          {...register('verifyPassword', {
            required: true,
            validate: (val: string) => val === watch('password'),
          })}
        />
      </Form.Group>

      <Button
        variant={isValid ? 'primary' : 'danger'}
        type="submit"
        disabled={!isValid}
      >
        Submit
      </Button>
    </Form>
  );
};

export default Register;

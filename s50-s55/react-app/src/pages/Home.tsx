import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const Home = () => {
  return (
    <>
      <Banner>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere.</p>
        <Link to="/courses">
          <Button>Enroll Now</Button>
        </Link>
      </Banner>
      <Highlights />
    </>
  );
};

export default Home;

import Swal from 'sweetalert2';

export const loginSuccess = () =>
  Swal.fire({
    title: 'Login Successful',
    icon: 'success',
    text: 'Welcome to Zuitt!',
  });

export const loginFailed = () =>
  Swal.fire({
    title: 'Authentication Failed',
    icon: 'error',
    text: 'Please, check your login details and try again.',
  });

export const enrollSuccess = () =>
  Swal.fire({
    title: 'Successfully Enrolled',
    icon: 'success',
    text: 'You have successfully enrolled for this course.',
  });

export const enrollFailed = () =>
  Swal.fire({
    title: 'Something went wrong',
    icon: 'error',
    text: 'Please try again.',
  });

export const emailExists = () =>
  Swal.fire({
    title: 'Duplicate Email Found',
    icon: 'error',
    text: 'Kindly provide another email to complete registration.',
  });

export const registerSuccessfull = () =>
  Swal.fire({
    title: 'Registration Successful',
    icon: 'success',
    text: 'Welcome to Zuitt!',
  });

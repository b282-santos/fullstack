import { CanceledError } from 'axios';
import { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';
import NavBar from './components/NavBar';
import { getUserInfo } from './services/userService';
import useAuthStore from './stores/authStore';

const App = () => {
  const { login } = useAuthStore();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) return;

    const { request, cancel } = getUserInfo(token as string);
    request
      .then(user => login(user))
      .catch(err => {
        if (err instanceof CanceledError) return;
      });

    return () => cancel();
  }, [login]);

  return (
    <>
      <NavBar />
      <Container className="py-5">
        <Outlet />
      </Container>
    </>
  );
};

export default App;

import { ReactNode } from 'react';
import { Col, Row } from 'react-bootstrap';

interface Props {
  children: ReactNode;
}

const Banner = ({ children }: Props) => {
  return (
    <Row>
      <Col className="p-5">{children}</Col>
    </Row>
  );
};

export default Banner;

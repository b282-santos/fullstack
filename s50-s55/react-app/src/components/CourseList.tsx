import { Outlet } from 'react-router-dom';
import useCourses from '../hooks/useCourses';
import CourseCard from './CourseCard';

const CourseList = () => {
  const { data: courses } = useCourses();

  return (
    <>
      {courses?.map(course => (
        <CourseCard key={course._id} course={course} />
      ))}
      <Outlet />
    </>
  );
};

export default CourseList;

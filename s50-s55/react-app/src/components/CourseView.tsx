import { useEffect } from 'react';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import { getCourse } from '../services/courseService';
import { enroll } from '../services/userService';
import useAuthStore from '../stores/authStore';
import useCourseStore from '../stores/courseStore';
import { enrollFailed, enrollSuccess } from '../utils/modals';

const CourseView = () => {
  const params = useParams();
  const { course, setCourse } = useCourseStore();
  const { user } = useAuthStore();

  useEffect(() => {
    getCourse(params.id as string).then(course => setCourse(course));
  }, [params.id, setCourse]);

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{course.name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{course.description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {course.price}</Card.Text>
              <Card.Subtitle>Class Schedule:</Card.Subtitle>
              <Card.Text>8:00 AM - 5:00 PM</Card.Text>
              {user._id ? (
                <Button
                  variant="primary"
                  onClick={() =>
                    enroll(course._id)
                      .then(() => enrollSuccess())
                      .catch(() => enrollFailed())
                  }
                >
                  Enroll
                </Button>
              ) : (
                <Link to="/login">
                  <Button variant="danger">Log in to Enroll</Button>
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default CourseView;

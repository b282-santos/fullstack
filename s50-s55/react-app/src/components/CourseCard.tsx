import { Button, Card, Col, Row } from 'react-bootstrap';
import { Course } from '../services/courseService';
import { Link } from 'react-router-dom';

interface Props {
  course: Course;
}

const CourseCard = ({ course }: Props) => {
  const { name, description, price } = course;

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Link to={`/courses/${course._id}`}>
              <Button>Details</Button>
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
};

export default CourseCard;

import { Card } from 'react-bootstrap';
import { Highlight } from '../data/highlightsData';

interface Props {
  highlight: Highlight;
}

const HighlightCard = ({ highlight }: Props) => {
  return (
    <Card className="cardHighlight p-3">
      <Card.Body>
        <Card.Title>
          <h2>{highlight.title}</h2>
        </Card.Title>
        <Card.Text>{highlight.description}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default HighlightCard;

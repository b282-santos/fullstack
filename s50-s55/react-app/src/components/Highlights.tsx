import { Col, Row } from 'react-bootstrap';
import highlightsData from '../data/highlightsData';
import HighlightCard from './HighlightCard';

const Highlights = () => {
  return (
    <Row className="mt-3 mb-3">
      {highlightsData.map((highlight, index) => (
        <Col key={index} xs={12} md={4}>
          <HighlightCard highlight={highlight} />
        </Col>
      ))}
    </Row>
  );
};

export default Highlights;

const firstName = document.querySelector('#first-name') as HTMLInputElement;
const lastName = document.querySelector('#last-name') as HTMLInputElement;
const fullName = document.querySelector('#full-name') as HTMLParagraphElement;

firstName.addEventListener('input', displayFullName);
lastName.addEventListener('input', displayFullName);

function displayFullName(): void {
  fullName.textContent = `${firstName.value} ${lastName.value}`;
}

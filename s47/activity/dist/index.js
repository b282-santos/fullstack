"use strict";
const firstName = document.querySelector('#first-name');
const lastName = document.querySelector('#last-name');
const fullName = document.querySelector('#full-name');
firstName.addEventListener('input', displayFullName);
lastName.addEventListener('input', displayFullName);
function displayFullName() {
    fullName.textContent = `${firstName.value} ${lastName.value}`;
}

function countLetter(letter, sentence) {
  const isSingleCharacter = letter.length === 1;
  if (isSingleCharacter) return sentence.split(letter).length - 1;
}

function isIsogram(text) {
  return new Set(text.toLowerCase()).size === text.length;
}

function purchase(age, price) {
  if (age < 13) return;
  const discount = age >= 22 && age <= 64 ? 0 : price * 0.2;
  return (price - discount).toFixed(2);
}

function findHotCategories(items) {
  const hotCategories = new Set(
    items.filter(item => !item.stocks).map(item => item.category)
  );
  return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
  return candidateA.filter(voter => candidateB.includes(voter));
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};

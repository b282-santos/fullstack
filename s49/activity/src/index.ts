const formAdd = document.querySelector('#form-add') as HTMLFormElement;
const formEdit = document.querySelector('#form-edit') as HTMLFormElement;
const title = document.querySelector('#title') as HTMLInputElement;
const body = document.querySelector('#body') as HTMLInputElement;
const editId = document.querySelector('#edit-id') as HTMLInputElement;
const editTitle = document.querySelector('#edit-title') as HTMLInputElement;
const editBody = document.querySelector('#edit-body') as HTMLInputElement;
const updateBtn = document.querySelector('#btn-update') as HTMLButtonElement;
const postList = document.querySelector('#div-post-entries') as HTMLDivElement;

const BASE_URL = 'https://jsonplaceholder.typicode.com';

interface Post {
  id?: number;
  title: string;
  body: string;
  userId: number;
}

const getAllPosts = async (): Promise<Post[]> => {
  const res = await fetch(BASE_URL + '/posts');
  return res.json();
};

const createPost = async (newPost: Post): Promise<Post> => {
  const init = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(newPost),
  };

  const res = await fetch(BASE_URL + '/posts', init);
  return res.json();
};

const updatePost = async (updatedPost: Post): Promise<Post> => {
  const init = {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(updatedPost),
  };

  const res = await fetch(BASE_URL + '/posts/' + updatedPost.id, init);
  return res.json();
};

const removePost = async (id: string): Promise<void> => {
  const init = { method: 'DELETE' };
  await fetch(BASE_URL + '/posts/' + id, init);
};

const editPost = async (id: string): Promise<void> => {
  const posts = await getAllPosts();
  const post = posts.find(p => p.id === Number(id));
  editId.value = post?.id?.toString() as string;
  editTitle.value = post?.title as string;
  editBody.value = post?.body as string;
  updateBtn.removeAttribute('disabled');
};

const deletePost = (id: string): void => {
  removePost(id);
  document.querySelector(`#post-${id}`)?.remove();
};

const renderPosts = async (): Promise<void> => {
  const posts = await getAllPosts();

  const postElement = posts
    .map(
      post => `
        <div id ="post-${post.id}">
          <h3 id="post-title-${post.id}">${post.title}</h3>
          <p id="post-body-${post.id}">${post.body}</p>
          <button onClick="editPost('${post.id}')">Edit</button>
          <button onClick="deletePost('${post.id}')">Delete</button>
        </div>
      `
    )
    .join('');

  postList.innerHTML = postElement;
};

formAdd.addEventListener('submit', async (e): Promise<void> => {
  e.preventDefault();

  const newPost = await createPost({
    title: title.value,
    body: body.value,
    userId: 1,
  });

  console.log(newPost);
  alert('Successfully added!');
  formAdd.reset();
});

formEdit.addEventListener('submit', async (e): Promise<void> => {
  e.preventDefault();

  const updatedPost = await updatePost({
    id: Number(editId.value),
    title: editTitle.value,
    body: editBody.value,
    userId: 1,
  });

  console.log(updatedPost);
  updateBtn.setAttribute('disabled', 'disabled');
  alert('Successfully updated!');
  formEdit.reset();
});

renderPosts();
